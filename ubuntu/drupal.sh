# installing mkcert
sudo apt install libnss3-tools
brew install mkcert

# Initialize mkcert
mkcert -install

# install ddev
snap install curl
## Add DDEV’s GPG key to your keyring
curl -fsSL https://pkg.ddev.com/apt/gpg.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/ddev.gpg > /dev/null

## Add DDEV releases to your package repository
echo "deb [signed-by=/etc/apt/trusted.gpg.d/ddev.gpg] https://pkg.ddev.com/apt/ * *" | sudo tee /etc/apt/sources.list.d/ddev.list >/dev/null

## Update package information and install DDEV
sudo apt update && sudo apt install -y ddev


